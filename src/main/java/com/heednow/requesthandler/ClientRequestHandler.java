package com.heednow.requesthandler;

import com.heednow.bo.ClientRequestBO;
import com.heednow.bo.UpdateClientBO;
import com.heednow.dao.ClientDAO;
import com.heednow.dao.user.UsersDAO;
import com.heednow.dto.client.ClientDTO;
import com.heednow.dto.response.LoginResponseDTO;
import com.heednow.response.client.ClientDetail;
import com.heednow.response.client.GetClientResponse;
import com.heednow.util.EmailService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import static com.heednow.requesthandler.DeviceRequestHandler.random;

/**
 * Created by System-2 on 3/9/2017.
 */
public class ClientRequestHandler {
    public List<ClientDetail> getClients() throws SQLException {
        ClientDAO clientDAO = new ClientDAO();
        List<ClientDetail> clients = getResponseFromDto(clientDAO.getClients());
        return clients;
    }

    public List<ClientDetail> getResponseFromDto(List<ClientDTO> clientDTOS) throws SQLException {
        List<ClientDetail> clientDetails = new ArrayList<ClientDetail>();
        Iterator<ClientDTO> iterator = clientDTOS.iterator();
        while (iterator.hasNext()) {
            ClientDTO clientDTO = iterator.next();
            ClientDetail answerResponseList = new ClientDetail(clientDTO.getId(),
                    clientDTO.getName(),
                    clientDTO.getEmail(),
                    clientDTO.getMobile(),
                    clientDTO.getLocation(),
                    clientDTO.getClientId(),
                    clientDTO.getNoOfOutlets(),
                    clientDTO.getNoOfDevices(),
                    clientDTO.getStatus());
            clientDetails.add(answerResponseList);
        }
        return clientDetails;
    }

    public GetClientResponse getClient(int id) throws SQLException {
        ClientDAO clientDAO = new ClientDAO();
        ClientDTO clientDTO = clientDAO.getClientById(id);
        GetClientResponse clientResponse = new GetClientResponse(clientDTO.getId(),
                clientDTO.getNoOfOutlets(),
                clientDTO.getNoOfDevices(),
                clientDTO.getName(),
                clientDTO.getEmail(),
                clientDTO.getMobile(),
                clientDTO.getLocation(),
                clientDTO.getClientId(),
                clientDTO.getStatus());
        return clientResponse;
    }

    public String addClient(ClientRequestBO clientRequesttBO) throws Exception {
        ClientDTO clientDTO = new ClientDTO();
        ClientDAO clientDAO = new ClientDAO();

        clientDTO.setName(clientRequesttBO.getName());
        clientDTO.setEmail(clientRequesttBO.getEmail());
        clientDTO.setMobile(clientRequesttBO.getMobile());
        clientDTO.setLocation(clientRequesttBO.getLocation());
        clientDTO.setUserName(clientRequesttBO.getUserName());
        clientDTO.setPassword(clientRequesttBO.getPassword());
        clientDTO.setClientId(generatePIN(clientRequesttBO.getName()));

        String id = clientDAO.createClient(clientDTO);
        ClientDTO clientDTO1 = clientDAO.getClient(id);
        clientDTO1.setUserName(clientRequesttBO.getUserName());
        clientDTO1.setPassword(clientRequesttBO.getPassword());
        EmailService.userRegister(clientDTO.getEmail(), clientDTO1);
        return id;
    }



    public String generatePIN(String input) {


        Random r = new Random();
        int otp = r.nextInt(99 - 10) + 10;
        StringBuilder sb;
        if(input.length()>8){
            sb = new StringBuilder(input.substring(0, 8).trim());
        }else{
            sb=new StringBuilder(input.trim());
        }
        String clientId = sb.toString() + otp;
        String strWithoutSpace = clientId.replaceAll("\\s", "").toUpperCase();
        return strWithoutSpace;

    }

    public Boolean updateClient(UpdateClientBO clientRequesttBO) throws SQLException {
        ClientDTO clientDTO = new ClientDTO();
        ClientDAO clientDAO = new ClientDAO();

        clientDTO.setName(clientRequesttBO.getName());
        clientDTO.setStatus(clientRequesttBO.getStatus());
        clientDTO.setEmail(clientRequesttBO.getEmail());
        clientDTO.setMobile(clientRequesttBO.getMobile());
        clientDTO.setLocation(clientRequesttBO.getLocation());
        clientDTO.setId(clientRequesttBO.getId());
        clientDTO.setNoOfOutlets(clientRequesttBO.getNoOfOutlets());
        clientDTO.setNoOfDevices(clientRequesttBO.getNoOfDevices());

        Boolean isProcessed = clientDAO.updateClient(clientDTO);
        return isProcessed;
    }


    public Boolean verifyEmail(String email) throws SQLException {

        Boolean isProcessed = Boolean.FALSE;
        ClientDAO clientDAO = new ClientDAO();
        try {
            if (!email.trim().equals("")) {
                isProcessed = clientDAO.getValidationForEmail(email);
            } else {
                return false;
            }
        } catch (SQLException sq) {
            isProcessed = false;
        }
        return isProcessed;
    }

    public Boolean verifyPhoneNumber(String mobile) throws SQLException{
        Boolean isProcessed = Boolean.FALSE;
        ClientDAO clientDAO = new ClientDAO();
        try {
            isProcessed = clientDAO.getValidationForPhoneNumber(mobile);
        } catch (SQLException sq) {
            isProcessed = false;
        }
        return isProcessed;
    }
}







