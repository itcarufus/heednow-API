package com.heednow.dao;

import com.heednow.dao.Sync.SmsDAO;
import com.heednow.dao.user.UsersDAO;
import com.heednow.dto.client.ClientDTO;
import com.heednow.dto.request.CreateRoleDTO;
import com.heednow.dto.request.SettingRequestDTO;
import com.heednow.dto.request.SmsSettingDTO;
import com.heednow.dto.response.LoginResponseDTO;
import com.heednow.util.CommaSeparatedString;
import com.heednow.util.ConnectionPool;
import com.heednow.util.DateUtil;
import com.heednow.util.MD5Encode;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by System-2 on 3/10/2017.
 */
public class ClientDAO {
    public String createClient(ClientDTO clientDTO) throws Exception {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        StringBuilder query = new StringBuilder("INSERT INTO heednow_clients(name , email, mobile, location,client_id) values (?,?,?,?,?)");
        String id = "";
        try {
            int parameterIndex = 1;
            connection = new ConnectionHandler().getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(query.toString());

            preparedStatement.setString(parameterIndex++, clientDTO.getName());

            preparedStatement.setString(parameterIndex++, clientDTO.getEmail());

            preparedStatement.setString(parameterIndex++, clientDTO.getMobile());

            preparedStatement.setString(parameterIndex++, clientDTO.getLocation());

            preparedStatement.setString(parameterIndex++, clientDTO.getClientId());

            int i = preparedStatement.executeUpdate();
            if (i > 0) {
                connection.commit();
            } else {
                connection.rollback();
            }

            try {
                ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    int id1 = generatedKeys.getInt(1);
                    UsersDAO usersDAO = new UsersDAO();
                    SmsDAO smsDAO = new SmsDAO();
                    id=getClientById(id1).getClientId();
                    CreateRoleDTO createRoleDTO = new CreateRoleDTO();
                    createRoleDTO.setIsAll(1);
                    createRoleDTO.setOutletAccess("");
                    createRoleDTO.setMenuAccess("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30");
                    createRoleDTO.setName("SUPER ADMIN");
                    int roleId=usersDAO.createRoll(createRoleDTO,id1);

                    LoginResponseDTO dto = new LoginResponseDTO();
                    dto.setUserName(clientDTO.getUserName());
                    dto.setEmail(clientDTO.getEmail());
                    dto.setPassword(MD5Encode.Encode(clientDTO.getPassword()));
                    dto.setRoleId(roleId);
                    dto.setNotifyEmail(1);
                    usersDAO.createUser(dto,id1);

                    SmsSettingDTO smsSettingDTO = new SmsSettingDTO();
                    smsSettingDTO.setApi("114370AghLwsC3U5749b2c9");
                    smsSettingDTO.setSenderId("HEEDNW");
                    smsSettingDTO.setCampaign(clientDTO.getClientId());
                    smsSettingDTO.setCountryCode("91");
                    smsSettingDTO.setName("India");
                    smsSettingDTO.setClientId(id1);
                    smsDAO.saveSmsSettings(smsSettingDTO);

                    DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                    Time timeValue = new java.sql.Time(formatter.parse("09:10:00").getTime());
                    Time timeValue1 = new java.sql.Time(formatter.parse("10:10:00").getTime());

                    SettingRequestDTO settingRequestDTO = new SettingRequestDTO();
                    settingRequestDTO.setArchiveTime(timeValue);
                    settingRequestDTO.setReportTime(timeValue1);
                    settingRequestDTO.setSmsTemplate("Negative feedback registered by %cn% (%cm%) for table number %tn% on %fd%. More details %url%");
                    settingRequestDTO.setEmailTemplate("Hello %n% You have requested to  register your device for %N%. Kindly  see the otp mentioned below.One Time Password : %o% Note: If you haven't requested this, please contact us immediately.Thanks,%N%");
                    settingRequestDTO.setVersionCode(2);
                    settingRequestDTO.setVersionName("1.1");
                    settingRequestDTO.setClientId(id1);
                    smsDAO.saveSetting(settingRequestDTO);


                } else {
                    throw new SQLException("Creating answer failed, no ID obtained.");
                }
            } catch (SQLException e) {
                connection.rollback();
                e.printStackTrace();
                throw e;
            }
        } catch (SQLException e) {
            connection.rollback();
            e.printStackTrace();
            throw e;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return id;
    }

    public List<ClientDTO> getClients() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        List<ClientDTO> clients = new ArrayList<ClientDTO>();
        try {
            connection = new ConnectionHandler().getConnection();
            statement = connection.createStatement();
            String query = "SELECT * from heednow_clients";
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                ClientDTO clientDTO = new ClientDTO();
                clientDTO.setName(resultSet.getString("name"));
                clientDTO.setId(resultSet.getInt("id"));
                clientDTO.setEmail(resultSet.getString("email"));
                clientDTO.setMobile(resultSet.getString("mobile"));
                clientDTO.setStatus(resultSet.getString("status"));
                clientDTO.setLocation(resultSet.getString("location"));
                clientDTO.setClientId(resultSet.getString("client_id"));
                clientDTO.setNoOfOutlets(resultSet.getInt("no_of_outlets"));
                clientDTO.setNoOfDevices(resultSet.getInt("no_of_devices"));
                clients.add(clientDTO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                statement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return clients;
    }

    public ClientDTO getClientById(int id) throws SQLException {
        Connection connection = null;
        Statement statement = null;
        ClientDTO clientDTO = new ClientDTO();
        try {

            connection = new ConnectionHandler().getConnection();
            statement = connection.createStatement();
            StringBuilder query = new StringBuilder("SELECT * from heednow_clients where id="+id);
            ResultSet resultSet = statement.executeQuery(query.toString());
            while (resultSet.next()) {
                clientDTO.setName(resultSet.getString("name"));
                clientDTO.setId(resultSet.getInt("id"));
                clientDTO.setEmail(resultSet.getString("email"));
                clientDTO.setMobile(resultSet.getString("mobile"));
                clientDTO.setStatus(resultSet.getString("status"));
                clientDTO.setLocation(resultSet.getString("location"));
                clientDTO.setClientId(resultSet.getString("client_id"));
                clientDTO.setNoOfOutlets(resultSet.getInt("no_of_outlets"));
                clientDTO.setNoOfDevices(resultSet.getInt("no_of_devices"));
                clientDTO.setDate(DateUtil.getCurrentServerTimeByRemoteTimestamp(resultSet.getTimestamp("created_date")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                statement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return clientDTO;
    }

    public static ClientDTO getClient(String id) throws SQLException {
        Connection connection = null;
        Statement statement = null;
        ClientDTO clientDTO = new ClientDTO();
        try {

            connection = new ConnectionHandler().getConnection();
            statement = connection.createStatement();
            StringBuilder query = new StringBuilder("SELECT * from heednow_clients where client_id='"+id+"'");
            ResultSet resultSet = statement.executeQuery(query.toString() .trim());
            while (resultSet.next()) {
                clientDTO.setName(resultSet.getString("name"));
                clientDTO.setId(resultSet.getInt("id"));
                clientDTO.setEmail(resultSet.getString("email"));
                clientDTO.setMobile(resultSet.getString("mobile"));
                clientDTO.setStatus(resultSet.getString("status"));
                clientDTO.setLocation(resultSet.getString("location"));
                clientDTO.setClientId(resultSet.getString("client_id"));
                clientDTO.setNoOfOutlets(resultSet.getInt("no_of_outlets"));
                clientDTO.setNoOfDevices(resultSet.getInt("no_of_devices"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                statement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return clientDTO;
    }


    public Boolean updateClient(ClientDTO clientDTO) throws SQLException {
        boolean isCreated = false;
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            int parameterIndex = 1;
            connection = new ConnectionHandler().getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement("UPDATE heednow_clients SET name=?, email =?, mobile =?, status=?, location=?, no_of_outlets=?,no_of_devices=? WHERE id =?");

            preparedStatement.setString(parameterIndex++, clientDTO.getName());

            preparedStatement.setString(parameterIndex++, clientDTO.getEmail());

            preparedStatement.setString(parameterIndex++, clientDTO.getMobile());

            preparedStatement.setString(parameterIndex++, clientDTO.getStatus());

            preparedStatement.setString(parameterIndex++, clientDTO.getLocation());

            preparedStatement.setInt(parameterIndex++, clientDTO.getNoOfOutlets());

            preparedStatement.setInt(parameterIndex++, clientDTO.getNoOfDevices());

            preparedStatement.setInt(parameterIndex++, clientDTO.getId());

            int i = preparedStatement.executeUpdate();
            if (i > 0) {
                connection.commit();
                isCreated = Boolean.TRUE;
            } else {
                connection.rollback();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return isCreated;
    }

    public Boolean getValidationForEmail(String email) throws SQLException {
        Connection connection = null;
        Statement statement = null;
        Boolean isProcessed = Boolean.FALSE;
        try {
            connection = new ConnectionPool().getConnection();
            statement = connection.createStatement();
            StringBuilder query = new StringBuilder(
                    "SELECT email FROM heednow_clients where email = \"")
                    .append(email).append("\"");
            ResultSet resultSet = statement.executeQuery(query.toString());

            while (resultSet.next()) {

                isProcessed = true;

            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } finally {
            try {
                statement.close();
                connection.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return isProcessed;
    }

    public Boolean getValidationForPhoneNumber(String mobile) throws SQLException {
        Connection connection = null;
        Statement statement = null;
        Boolean isProcessed = Boolean.FALSE;
        try {

            connection = new ConnectionPool().getConnection();
            statement = connection.createStatement();
            StringBuilder query = new StringBuilder(
                    "SELECT mobile FROM heednow_clients where mobile = \"")
                    .append(mobile).append("\"");
            ResultSet resultSet = statement.executeQuery(query.toString());

            while (resultSet.next()) {

                isProcessed = true;
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } finally {
            try {
                statement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return isProcessed;
    }


    public Boolean getExpireDate(ClientDTO client) throws SQLException {
        Connection connection = null;
        Statement statement = null;
        Boolean isProcessed = Boolean.FALSE;
        try {
            connection = new ConnectionPool().getConnection();
            statement = connection.createStatement();
            StringBuilder query = new StringBuilder(
                    "SELECT * FROM heednow_clients\n" +
                            "WHERE id="+client.getId()+" and '"+client.getDate()+"' > NOW()-INTERVAL 14 DAY");
            ResultSet resultSet = statement.executeQuery(query.toString());

            while (resultSet.next()) {

                isProcessed = true;
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } finally {
            try {
                statement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return isProcessed;
    }
}
