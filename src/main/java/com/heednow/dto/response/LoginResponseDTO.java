package com.heednow.dto.response;

public class LoginResponseDTO {
    private int id;
    private String userName;
    private String name;
    private String password;
    private String email;
    private String status;
    private String outletAccess;
    private String sessionId;
    private String menuAccess;
    private int roleId;
    private int clientId;
    private int notifyEmail;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOutletAccess() {
        return outletAccess;
    }

    public void setOutletAccess(String outletAccess) {
        this.outletAccess = outletAccess;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getMenuAccess() {
        return menuAccess;
    }

    public void setMenuAccess(String menuAccess) {
        this.menuAccess = menuAccess;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getNotifyEmail() {
        return notifyEmail;
    }

    public void setNotifyEmail(int notifyEmail) {
        this.notifyEmail = notifyEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoginResponseDTO that = (LoginResponseDTO) o;

        if (id != that.id) return false;
        if (roleId != that.roleId) return false;
        if (clientId != that.clientId) return false;
        if (notifyEmail != that.notifyEmail) return false;
        if (!userName.equals(that.userName)) return false;
        if (!name.equals(that.name)) return false;
        if (!password.equals(that.password)) return false;
        if (!email.equals(that.email)) return false;
        if (!status.equals(that.status)) return false;
        if (!outletAccess.equals(that.outletAccess)) return false;
        if (!sessionId.equals(that.sessionId)) return false;
        return menuAccess.equals(that.menuAccess);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + userName.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + status.hashCode();
        result = 31 * result + outletAccess.hashCode();
        result = 31 * result + sessionId.hashCode();
        result = 31 * result + menuAccess.hashCode();
        result = 31 * result + roleId;
        result = 31 * result + clientId;
        result = 31 * result + notifyEmail;
        return result;
    }

    @Override
    public String toString() {
        return "LoginResponseDTO{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", status='" + status + '\'' +
                ", outletAccess='" + outletAccess + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", menuAccess='" + menuAccess + '\'' +
                ", roleId=" + roleId +
                ", clientId=" + clientId +
                ", notifyEmail=" + notifyEmail +
                '}';
    }
}
