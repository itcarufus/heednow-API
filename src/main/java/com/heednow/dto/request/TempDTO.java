package com.heednow.dto.request;

/**
 * Created by System-2 on 12/20/2016.
 */
public class TempDTO
{
    private int outletId;
    private String status;
    private String desc;
    private int templateId;

    public int getOutletId() {
        return outletId;
    }

    public void setOutletId(int outletId) {
        this.outletId = outletId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getTemplateId() {
        return templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TempDTO tempDTO = (TempDTO) o;

        if (outletId != tempDTO.outletId) return false;
        if (templateId != tempDTO.templateId) return false;
        if (status != null ? !status.equals(tempDTO.status) : tempDTO.status != null) return false;
        return desc != null ? desc.equals(tempDTO.desc) : tempDTO.desc == null;
    }

    @Override
    public int hashCode() {
        int result = outletId;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (desc != null ? desc.hashCode() : 0);
        result = 31 * result + templateId;
        return result;
    }

    @Override
    public String toString() {
        return "TempDTO{" +
                "outletId=" + outletId +
                ", status='" + status + '\'' +
                ", desc='" + desc + '\'' +
                ", templateId=" + templateId +
                '}';
    }
}
