package com.heednow.request.outlet;

/**
 * Created by System-2 on 12/20/2016.
 */
public class AssignTemplateRequest
{
    private int templateId;

    public int getTemplateId() {
        return templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AssignTemplateRequest that = (AssignTemplateRequest) o;

        return templateId == that.templateId;
    }

    @Override
    public int hashCode() {
        return templateId;
    }

    @Override
    public String toString() {
        return "AssignTemplateRequest{" +
                "templateId=" + templateId +
                '}';
    }
}
