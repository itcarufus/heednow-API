package com.heednow.bo;

/**
 * Created by System-2 on 2/9/2017.
 */
public class UpdateUserRequestBO {
    private int id;
    private String email;
    private String name;
    private String status;
    private String role;
    private int notifyEmail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getNotifyEmail() {
        return notifyEmail;
    }

    public void setNotifyEmail(int notifyEmail) {
        this.notifyEmail = notifyEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UpdateUserRequestBO that = (UpdateUserRequestBO) o;

        if (id != that.id) return false;
        if (notifyEmail != that.notifyEmail) return false;
        if (!email.equals(that.email)) return false;
        if (!name.equals(that.name)) return false;
        if (!status.equals(that.status)) return false;
        return role.equals(that.role);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + email.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + status.hashCode();
        result = 31 * result + role.hashCode();
        result = 31 * result + notifyEmail;
        return result;
    }

    @Override
    public String toString() {
        return "UpdateUserRequestBO{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", role='" + role + '\'' +
                ", notifyEmail=" + notifyEmail +
                '}';
    }
}
