package com.heednow.response.template;

import com.heednow.response.util.GenericResponse;

/**
 * Created by System-2 on 1/7/2017.
 */
public class GetTemplateResponse implements GenericResponse {
    private int id;
    private String desc;
    private String status;
    private String message;
    private String messageType;
    private int outletId;

    public GetTemplateResponse(int id, String desc, String status,int outletId) {
        this.id = id;
        this.desc = desc;
        this.status = status;
        this.outletId = outletId;
    }

    public int getId() {
        return id;
    }

    public String getDesc() {
        return desc;
    }

    public String getStatus() {
        return status;
    }

    public int getOutletId() {
        return outletId;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageType() {
        return messageType;
    }

    @Override
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    @Override
    public String toString() {
        return "GetTemplateResponse{" +
                "id=" + id +
                ", desc='" + desc + '\'' +
                ", status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", messageType='" + messageType + '\'' +
                ", outletId=" + outletId +
                '}';
    }
}

